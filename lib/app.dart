import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:test_project/core/app_locale.dart';
import 'package:test_project/core/app_theme.dart';
import 'package:test_project/modules/home_module/screen.dart';
import 'package:test_project/setup.dart';
import 'package:test_project/widgets/splash_screen.dart';
import 'package:test_project/widgets/system_theme_update_listener.dart';

import 'core/app_router.dart';
import 'cubits/app/app_cubit.dart';

class App extends StatelessWidget {
  static BuildContext? context;
  static final navigatorKey = GlobalKey<NavigatorState>();

  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return SetupApp(
      builder: (context) {
        return SystemThemeUpdateListener(
          child: MaterialApp(
            builder: (BuildContext context, Widget? child) {
              return child!;
            },
            home: const _StartScreen(),
            locale: context.locale,
            navigatorKey: navigatorKey,
            debugShowCheckedModeBanner: false,
            supportedLocales: context.supportedLocales,
            onGenerateRoute: AppRouter.onGenerateRoute,
            localizationsDelegates: AppLocale.localizationsDelegates,
            themeMode: context.watch<AppCubit>().state.themeMode,
            theme: AppTheme.lightTheme,
            darkTheme: AppTheme.darkTheme,
          ),
        );
      },
    );
  }
}

class _StartScreen extends StatefulWidget {
  const _StartScreen();

  @override
  State<_StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<_StartScreen> {
  bool _previewVisible = true;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3)).then(
      (_) => setState(() => _previewVisible = false),
    );
  }

  Widget _buildChild(BuildContext context) {
    return const HomeScreen();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    App.context = context;

    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 350),
      child: _previewVisible ? SplashScreen.animated() : _buildChild(context),
    );
  }
}
