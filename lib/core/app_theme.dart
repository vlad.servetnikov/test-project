import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:test_project/core/app_bloc.dart';

class AppColorSchema {
  final Color primaryBgColor;
  final Color secondaryBgColor;
  final Brightness brightness;

  const AppColorSchema({
    required this.brightness,
    required this.primaryBgColor,
    required this.secondaryBgColor,
  });
}

class AppTheme {
  static const AppColorSchema lightColors = AppColorSchema(
    brightness: Brightness.light,
    primaryBgColor: Color.fromRGBO(231, 238, 249, 1),
    secondaryBgColor: Color.fromRGBO(255, 255, 255, 1),
  );

  static const AppColorSchema darkColors = AppColorSchema(
    brightness: Brightness.dark,
    primaryBgColor: Color.fromRGBO(11, 24, 46, 1),
    secondaryBgColor: Color.fromRGBO(8, 18, 34, 1),
  );

  static void toggleTheme() => AppBloc.appCubit
      .changeThemeMode(isDarkMode ? ThemeMode.light : ThemeMode.dark);

  static ThemeMode get platformThemeMode =>
      SchedulerBinding.instance.window.platformBrightness == Brightness.dark
          ? ThemeMode.dark
          : ThemeMode.light;

  static ThemeMode get currentThemeMode {
    final stateMode = AppBloc.appCubit.state.themeMode;
    if (stateMode == ThemeMode.system) return platformThemeMode;
    return stateMode;
  }

  static bool get isDarkMode => currentThemeMode == ThemeMode.dark;

  static SystemUiOverlayStyle get overlayStyle =>
      isDarkMode ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark;

  static Color invert(Color darkColor, Color lightColor) =>
      isDarkMode ? darkColor : lightColor;

  static AppColorSchema get colors => isDarkMode ? darkColors : lightColors;

  static ThemeData get lightTheme => buildTheme(lightColors);

  static ThemeData get darkTheme => buildTheme(darkColors);

  static ThemeData buildTheme(AppColorSchema colors) {
    return ThemeData();
  }
}
