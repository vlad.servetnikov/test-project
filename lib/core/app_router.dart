// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_project/modules/home_module/screen.dart';
import 'package:test_project/utils/screen_transitions.dart';

class AppRouter {
  static Map<String, WidgetBuilder> routes = {
    HomeScreen.path: (context) => HomeScreen(),
  };

  static Route onGenerateRoute(RouteSettings settings) {
    return CustomCupertinoRoute(
      settings: settings,
      builder: (context) => routes[settings.name]!(context),
    );
  }
}
