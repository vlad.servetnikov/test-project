import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_project/core/app_bloc.dart';
import 'package:test_project/core/http_client.dart';
import 'package:test_project/widgets/splash_lifecycle_overlay.dart';
import 'package:test_project/widgets/splash_screen.dart';

import 'core/app_locale.dart';

class SetupApp extends StatefulWidget {
  final Widget Function(BuildContext) builder;

  const SetupApp({required this.builder, super.key});

  @override
  _SetupAppState createState() => _SetupAppState();
}

class _SetupAppState extends State<SetupApp> {
  final Future<void> _setupFuture = _setup();

  @override
  void dispose() {
    AppBloc.appCubit.onCloseApp();
    super.dispose();
  }

  static Future<void> _setup() async {
    AppBloc.init();
    await HttpClient.init();
    AppBloc.appCubit.onStartApp();
  }

  @override
  Widget build(BuildContext context) {
    return AppLocale.localizeApp(
      app: ScreenUtilInit(
        minTextAdapt: true,
        designSize: const Size(375, 812),
        builder: (_, __) {
          return FutureBuilder(
            future: _setupFuture,
            builder: (context, snapshot) {
              Intl.defaultLocale = context.locale.languageCode;
              if (snapshot.connectionState != ConnectionState.done) {
                return Container();
              }

              return SplashLifecycleOverlay(
                splashBuilder: (_) => const SplashScreen(),
                child: AppBloc.appBuilder(widget.builder),
              );
            },
          );
        },
      ),
    );
  }
}
