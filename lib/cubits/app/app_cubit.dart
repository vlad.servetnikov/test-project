import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:test_project/core/app_locale.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> with HydratedMixin {
  AppCubit() : super(const AppState());

  void onStartApp() {}

  void onCloseApp() {}

  Future<void> changeLanguage(String language) async {
    await AppLocale.changeLanguage(language);
  }

  Future<void> logout() async {
    try {
      AppLocale.resetToSystemLanguage();
    } finally {
      emit(AppState(themeMode: state.themeMode));
    }
  }

  void changeThemeMode(ThemeMode mode) {
    emit(state.copyWith(themeMode: mode));
  }

  @override
  Map<String, dynamic> toJson(AppState state) => state.toJson();

  @override
  AppState fromJson(Map<String, dynamic> json) {
    try {
      return AppState.fromJson(json);
    } catch (_) {}

    return const AppState();
  }
}
