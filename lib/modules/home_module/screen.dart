import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/modules/home_module/cubit.dart';
import 'package:test_project/modules/home_module/state.dart';
import 'package:video_player/video_player.dart';

class HomeScreen extends StatelessWidget {
  static const path = 'home';

  const HomeScreen({super.key});

  Widget _buildVideoPlayer(VideoPlayerController controller) {
    return AspectRatio(
      aspectRatio: controller.value.aspectRatio,
      child: VideoPlayer(controller),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit()..init(),
      child: BlocBuilder<HomeCubit, HomeState>(builder: (context, state) {
        final cubit = context.watch<HomeCubit>();

        return Scaffold(
          appBar: AppBar(
            title: Text('title'.tr()),
          ),
          body: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              if (state.isLoading) {
                return const Center(child: CircularProgressIndicator());
              }

              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(child: _buildVideoPlayer(cubit.controller1)),
                    Expanded(child: _buildVideoPlayer(cubit.controller2)),
                  ],
                ),
              );
            },
          ),
        );
      }),
    );
  }
}
