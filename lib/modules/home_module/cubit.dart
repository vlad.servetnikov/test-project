import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_project/modules/home_module/state.dart';
import 'package:video_player/video_player.dart';

const String _video1 =
    'https://dev-api-video.u-prox.systems:443/api/v1/event/test/playback/masterPlaylist.m3u8?streamNumber=1';
const String _video2 =
    'https://dev-api-video.u-prox.systems:443/api/v1/event/test/playback/masterPlaylist.m3u8?streamNumber=2';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(const HomeState());

  late VideoPlayerController controller1;
  late VideoPlayerController controller2;

  void init() {
    _initializeAndPlayVideo();
  }

  Future<void> _initializeAndPlayVideo() async {
    emit(state.copyWith(isLoading: true));

    controller1 = VideoPlayerController.networkUrl(Uri.parse(_video1));
    controller2 = VideoPlayerController.networkUrl(Uri.parse(_video2));

    controller1.setLooping(true);
    controller2.setLooping(true);

    await _initialize();

    if (controller1.value.isInitialized && controller2.value.isInitialized) {
      await _playVideos();

      emit(state.copyWith(isLoading: false));
    }
  }

  Future<void> _initialize() async {
    await controller1.initialize();
    await controller2.initialize();
  }

  Future<void> _playVideos() async {
    await controller1.play();
    await controller2.play();
  }
}
