import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rive/rive.dart';
import 'package:test_project/core/app_theme.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  static Widget animated() {
    return const SplashScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.colors.secondaryBgColor,
      height: ScreenUtil().screenHeight,
      width: ScreenUtil().screenWidth,
      child: const RiveAnimation.asset(
        'assets/riv/shapes.riv',
        fit: BoxFit.cover,
      ),
    );
  }
}
